import React from "react";
import "./Footer.css";
import { Button } from "./Button";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <div className="footer-container">
      <div className="flex-container">
        <section className="footer-subscription"></section>
        <section className="footer-subscription">
          <p className="footer-subscription-text">Want to stay in touch?</p>
          <p className="footer-subscription-heading">NEWSLETTER SUBSCRIBE</p>
          <p className="footer-subscription-text">
            In order to start receiving our news all you have to do is enter
            your email address. Everything else will no taken care of by us. We
            sill send you emails containing information about game. We don't
            spam.
          </p>
          <p className="footer-subscription-text-2">Your email address:</p>
          <div className="input-areas">
            <form>
              <input
                className="footer-input"
                name="email"
                type="email"
                placeholder="Your Email"
              />
              <Link to="/login" className="btn-mobile">
                <Button buttonStyle="btn--primary">Subscribe</Button>
              </Link>
            </form>
          </div>
        </section>
      </div>
      <section class="social-media">
        <div class="social-media-wrap">
          <div class="footer-logo">
            <Link to="/" className="social-logo">
              GOJEK
              <i class="fab fa-typo3" />
            </Link>
          </div>

          <div class="social-icons">
            <Link
              class="social-icon-link facebook"
              to="/"
              target="_blank"
              aria-label="Facebook"
            >
              <i class="fab fa-facebook-f" />
            </Link>
            <Link
              class="social-icon-link instagram"
              to="/"
              target="_blank"
              aria-label="Instagram"
            >
              <i class="fab fa-instagram" />
            </Link>
            <Link
              class="social-icon-link youtube"
              to="/"
              target="_blank"
              aria-label="Youtube"
            >
              <i class="fab fa-youtube" />
            </Link>
            <Link
              class="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="Twitter"
            >
              <i class="fab fa-twitter" />
            </Link>
            <Link
              class="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="LinkedIn"
            >
              <i class="fab fa-linkedin" />
            </Link>
          </div>
        </div>
        <hr></hr>
        <small class="website-rights">
          © Binar 2021, Inc. All Rights Reserved
        </small>
      </section>
    </div>
  );
}

export default Footer;
