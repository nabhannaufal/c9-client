import React from "react";
import ListGame from "../ListGame";
import NavbarUser from "../NavbarUser";

export default function RegisterPage() {
  return (
    <>
      <NavbarUser></NavbarUser>
      <ListGame></ListGame>
    </>
  );
}
