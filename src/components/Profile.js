import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import "./Profile.css";

function Profile() {
  const history = useHistory();
  const [username] = useState(localStorage.getItem("username") || null);
  const [token] = useState(localStorage.getItem("token") || null);
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const [bio, setBio] = useState("");
  // const [urlMedsos, setUrlMedsos] = useState("");
  const [totalScore, setTotalScore] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/auth/profil", {
        headers: { authorization: token },
      })
      .then(function (response) {
        // handle success
        console.log(response);
        setEmail(response.data.email);
        setCity(response.data.city);
        setBio(response.data.bio);
        // setUrlMedsos(response.data.social_media_url);
        setTotalScore(response.data.total_score);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
        history.push("/login");
      });
  });

  return (
    <div className="body-profile">
      <div className="profile-card">
        <header>
          <img
            src="https://static.vecteezy.com/system/resources/previews/002/275/847/original/male-avatar-profile-icon-of-smiling-caucasian-man-vector.jpg"
            alt=""
          ></img>
          <h1>{username}</h1>
          <h2>{email}</h2>
          <h3>Score: {totalScore}</h3>
        </header>
        <div class="profile-bio">
          <p>
            {bio}, asal dari {city}
          </p>
        </div>
      </div>
    </div>
  );
}

export default Profile;
