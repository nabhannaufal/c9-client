import React from "react";
import "./ListGame.css";
import logo from "./images/suwit/LogoSuwit.png";
import { Link } from "react-router-dom";

function ListGame() {
  return (
    <div className="body-profile">
      <div class="cards-list">
        <Link to="/suwit" class="card 1" style={{ textDecoration: "none" }}>
          <div class="card_image">
            <img alt="" src={logo} />
          </div>
          <div class="card_title glow">
            <p>Suwit</p>
          </div>
        </Link>

        <div class="card 2  comingsoon">
          <div class="card_image">
            <img alt="" src="https://i.redd.it/b3esnz5ra34y.jpg" />
          </div>
          <div class="card_title title-white">
            <p>Coming Soon!</p>
          </div>
        </div>

        <div class="card 3 comingsoon">
          <div class="card_image">
            <img
              alt=""
              src="https://media.giphy.com/media/10SvWCbt1ytWCc/giphy.gif"
            />
          </div>
          <div class="card_title ">
            <p>Coming Soon!</p>
          </div>
        </div>

        <div class="card 4 comingsoon">
          <div class="card_image">
            <img
              alt=""
              src="https://media.giphy.com/media/LwIyvaNcnzsD6/giphy.gif"
            />
          </div>
          <div class="card_title title-black">
            <p>Coming Soon!</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListGame;
