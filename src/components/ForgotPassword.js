import React from "react";
import "./Login.css";
import { Link } from "react-router-dom";

function ForgotPassword() {
  return (
    <div className="auth-container">
      <form className="auth-container-form">
        <h2 className="auth-h2">Reset Password!</h2>
        <fieldset className="auth-fieldset">
          <legend className="auth-legend">Password Reset</legend>
          <ul className="auth-ul">
            <li className="auth-li">
              <em className="auth-em">
                A reset link will be sent to your inbox!
              </em>
            </li>
            <li className="auth-li">
              <label className="auth-label" for="email">
                Email:
              </label>
              <input className="auth-input" type="email" id="email" required />
            </li>
          </ul>
        </fieldset>
        <Link className="auth-button" to="/forgotpassword">
          Send Reset Link
        </Link>
        <Link className="auth-button" type="button" to="/login">
          Go Back
        </Link>
      </form>
    </div>
  );
}

export default ForgotPassword;
