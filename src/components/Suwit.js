import React, { useState, useEffect } from "react";
import "./Suwit.css";
import crown from "./images/suwit/crown.png";
import gambarbatu from "./images/suwit/batu.png";
import gambarkertas from "./images/suwit/kertas.png";
import gambargunting from "./images/suwit/gunting.png";
import refresh from "./images/suwit/refresh.png";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";

function Suwit() {
  const history = useHistory();
  const [username] = useState(localStorage.getItem("username") || null);
  const [token] = useState(localStorage.getItem("token") || null);

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/auth/profil", {
        headers: { authorization: token },
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
        history.push("/login");
      });
  });

  let icon = ["batu", "kertas", "gunting"];
  let [winBoard, setWinBoard] = useState(false);
  let [textComp, setTextComp] = useState(false);
  let [textPlayer, setTextPlayer] = useState(false);
  let [textDraw, setTextDraw] = useState(false);
  let [batu, setBatu] = useState(false);
  let [kertas, setKertas] = useState(false);
  let [gunting, setGunting] = useState(false);
  let [batuComp, setBatuComp] = useState(false);
  let [kertasComp, setKertasComp] = useState(false);
  let [guntingComp, setGuntingComp] = useState(false);
  let [crownComp, setCrownComp] = useState(false);
  let [crownPlayer, setCrownPlayer] = useState(false);
  let [comScore, setComScore] = useState(0);
  let [playerScore, setPlayerScore] = useState(0);
  let [drawScore, setDrawScore] = useState(0);

  const reset = () => {
    setBatu(false);
    setKertas(false);
    setGunting(false);
    setBatuComp(false);
    setKertasComp(false);
    setGuntingComp(false);
  };

  const handleBatu = () => {
    reset();
    setBatu(true);
    setKertas(false);
    setGunting(false);
    suit("batu");
  };

  const handleKertas = () => {
    reset();
    setBatu(false);
    setKertas(true);
    setGunting(false);
    suit("kertas");
  };

  const handleGunting = () => {
    reset();
    setBatu(false);
    setKertas(false);
    setGunting(true);
    suit("gunting");
  };

  const handleReset = () => {
    reset();
    setComScore(0);
    setDrawScore(0);
    setPlayerScore(0);
    setTextComp(false);
    setTextDraw(false);
    setTextPlayer(false);
    setWinBoard("result-begin");
  };

  const suit = (choice) => {
    let playerChoice = choice;
    let randomNumber = Math.floor(Math.random() * 3);
    let comChoice = icon[randomNumber];
    if (comChoice === "batu") {
      setBatuComp(true);
      setKertasComp(false);
      setGuntingComp(false);
    } else if (comChoice === "kertas") {
      setBatuComp(false);
      setKertasComp(true);
      setGuntingComp(false);
    } else {
      setBatuComp(false);
      setKertasComp(false);
      setGuntingComp(true);
    }

    let result = winChecking(playerChoice, comChoice);
    if (result === "result-com-win") {
      setTextComp(true);
      setTextDraw(false);
      setTextPlayer(false);
      setComScore(comScore + 1);
      setCrownComp(true);
      setCrownPlayer(false);
    } else if (result === "result-player-win") {
      setTextComp(false);
      setTextDraw(false);
      setTextPlayer(true);
      setPlayerScore(playerScore + 1);
      setCrownComp(false);
      setCrownPlayer(true);
    } else {
      setTextComp(false);
      setTextDraw(true);
      setTextPlayer(false);
      setDrawScore(drawScore + 1);
      setCrownComp(false);
      setCrownPlayer(false);
    }

    setWinBoard(result);
  };

  function winChecking(playerChoice, comChoice) {
    if (playerChoice === comChoice) return "result-draw";

    if (playerChoice === "batu" && comChoice === "kertas") {
      return "result-com-win";
    }

    if (playerChoice === "batu" && comChoice === "gunting") {
      return "result-player-win";
    }

    if (playerChoice === "kertas" && comChoice === "batu") {
      return "result-player-win";
    }

    if (playerChoice === "kertas" && comChoice === "gunting") {
      return "result-player-win";
    }

    if (playerChoice === "gunting" && comChoice === "batu") {
      return "result-player-win";
    }

    if (playerChoice === "gunting" && comChoice === "kertas") {
      return "result-player-win";
    }
  }

  return (
    <div className="suwit-container">
      <div className="suwit-header">
        <Link to="/listgame" className="suwit-back-button"></Link>
        <div className="suwit-logo"></div>
        <h3>ROCK PAPPER SCISSOR</h3>
      </div>
      <div className="suwit-game">
        <div className="suwit-row">
          <div className="suwit-column-left">
            <img
              src={crown}
              alt=""
              className={crownPlayer ? "show-crown" : "hide-crown"}
            ></img>
            <p className="suwit-h1">
              <u>{username}</u>
            </p>
          </div>
          <div className="suwit-column-right">
            <img
              src={crown}
              alt=""
              className={crownComp ? "show-crown" : "hide-crown"}
            ></img>
            <p className="suwit-h1">
              <u>Computer</u>
            </p>
          </div>
        </div>
        <div className="suwit-row">
          <div className="suwit-column-left">
            <img
              id="batu"
              src={gambarbatu}
              alt=""
              className={batu ? "tangan active-pulse" : "tangan"}
              onClick={handleBatu}
            />
          </div>
          <div className="suwit-column-right">
            <img
              id="batu-comp"
              src={gambarbatu}
              alt=""
              className={
                batuComp ? "tangan active-pulse not-allow" : "tangan not-allow"
              }
            />
          </div>
        </div>
        <div className="suwit-row">
          <div className="suwit-column-left">
            <img
              id="kertas"
              src={gambarkertas}
              alt=""
              className={kertas ? "tangan active-pulse" : "tangan"}
              onClick={handleKertas}
            />
          </div>
          <div className="suwit-column-mid">
            <div id="versus-h1" className={winBoard}></div>
          </div>
          <div className="suwit-column-right">
            <img
              id="kertas-comp"
              src={gambarkertas}
              alt=""
              className={
                kertasComp
                  ? "tangan active-pulse not-allow"
                  : "tangan not-allow"
              }
            />
          </div>
        </div>
        <div className="suwit-row">
          <div className="suwit-column-left">
            <img
              id="gunting"
              src={gambargunting}
              alt=""
              className={gunting ? "tangan active-pulse" : "tangan"}
              onClick={handleGunting}
            />
          </div>
          <div className="suwit-column-right">
            <img
              id="gunting-comp"
              src={gambargunting}
              alt=""
              className={
                guntingComp
                  ? "tangan active-pulse not-allow"
                  : "tangan not-allow"
              }
            />
          </div>
        </div>
        <div className="suwit-row-2">
          <img
            id="reset"
            src={refresh}
            alt=""
            className="suwit-refresh"
            onClick={handleReset}
          />
        </div>
        <div className="suwit-row suwit-border">
          <div className="suwit-column-left">
            <p className="suwit-h3 ">Player Score:</p>
            <div className={textPlayer ? "text-red" : ""} id="player_score">
              {playerScore}
            </div>
          </div>
          <div className="suwit-column-mid">
            <p className="suwit-h3">Draw:</p>
            <div className={textDraw ? "text-red" : ""} id="draw_score">
              {drawScore}
            </div>
          </div>
          <div className="suwit-column-right">
            <p className="suwit-h3">Computer Score:</p>
            <div className={textComp ? "text-red" : ""} id="com_score">
              {comScore}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Suwit;
