import React, { useState } from "react";
import "./Login.css";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/auth/login", {
        username,
        password,
      })
      .then(function (response) {
        localStorage.setItem("token", response.data.accessToken);
        localStorage.setItem("username", response.data.username);
        console.log(response);
        history.push("/profile");
      })
      .catch(function (error) {
        console.log(error);
        alert("Username atau password salah");
      });
  };

  return (
    <div className="auth-container">
      <form className="auth-container-form" onSubmit={(e) => handleSubmit(e)}>
        <h2 className="auth-h2">Welcome Back!</h2>
        <fieldset className="auth-fieldset">
          <legend className="auth-legend">Log In</legend>
          <ul className="auth-ul">
            <li className="auth-li">
              <label className="auth-label" for="username">
                Username:
              </label>
              <input
                className="auth-input"
                type="text"
                id="username"
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </li>

            <li className="auth-li">
              <label className="auth-label" for="password">
                Password:
              </label>
              <input
                className="auth-input"
                type="password"
                id="password"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </li>
            <li className="auth-li">
              <Link to="/forgotpassword" className="auth-a">
                <i>Forgot Password? </i>
              </Link>
            </li>
          </ul>
        </fieldset>
        <button type="submit" className="auth-button">
          Login
        </button>
        <Link className="auth-button" type="button" to="/signup">
          Create an account?
        </Link>
      </form>
    </div>
  );
}

export default Login;
