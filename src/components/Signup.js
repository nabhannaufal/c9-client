import React, { useState } from "react";
import "./Login.css";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";

function Signup() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const history = useHistory();

  const handleRegistration = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/auth/register", {
        username,
        email,
        password,
        city,
      })
      .then(function (response) {
        console.log(response);
        history.push("/login");
        alert("anda berhasil daftar");
      })
      .catch(function (error) {
        console.log(error);
        alert("data yang dimasukan salah atau username sudah ada!");
      });
  };
  return (
    <div className="auth-container">
      <form
        className="auth-container-form"
        onSubmit={(e) => handleRegistration(e)}
      >
        <h2 className="auth-h2">Sign Up!</h2>
        <fieldset className="auth-fieldset">
          <legend className="auth-legend">Create Account</legend>
          <ul className="auth-ul">
            <li className="auth-li">
              <label className="auth-label" for="username">
                Username:
              </label>
              <input
                className="auth-input"
                type="text"
                id="username"
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </li>
            <li className="auth-li">
              <label className="auth-label" for="email">
                Email:
              </label>
              <input
                className="auth-input"
                type="email"
                id="email"
                required
                onChange={(e) => setEmail(e.target.value)}
              />
            </li>
            <li className="auth-li">
              <label className="auth-label" for="password">
                Password:
              </label>
              <input
                className="auth-input"
                type="password"
                id="password"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </li>
            <li className="auth-li">
              <label className="auth-label" for="city">
                City:
              </label>
              <input
                className="auth-input"
                type="text"
                id="city"
                required
                onChange={(e) => setCity(e.target.value)}
              />
            </li>
          </ul>
        </fieldset>
        <button type="submit" className="auth-button">
          SUBMIT
        </button>
        <Link className="auth-button" type="button" to="/login">
          HAVE AN ACCOUNT?
        </Link>
      </form>
    </div>
  );
}

export default Signup;
