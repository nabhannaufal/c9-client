import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LandingPage from "./components/pages/LandingPage";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import Work from "./components/pages/Work";
import Suwit from "./components/pages/SuwitPage";
import Login from "./components/pages/LoginPage";
import Signup from "./components/pages/RegisterPage";
import Forgot from "./components/pages/ForgotPasswordPage";
import Profile from "./components/pages/ProfilePage";
import ListGame from "./components/pages/ListGamesPage";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/suwit" exact component={Suwit} />
        <Route path="/profile" exact component={Profile} />
        <Route path="/listgame" exact component={ListGame} />
        <div>
          <Navbar />
          <Route path="/" exact component={LandingPage} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/work" component={Work} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={Login} />
          <Route path="/forgotpassword" component={Forgot} />
        </div>
      </Switch>
    </Router>
  );
}

export default App;
